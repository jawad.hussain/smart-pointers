#ifndef DATABASE_H
#define DATABASE_H

#include "student.h"
#include <memory>

namespace emumba::training
{
    class database
    {
    private:
        std::map<std::string, std::shared_ptr<emumba::training::student>> shared_ptr_data;

    public:
        database();
        ~database();
        const std::shared_ptr<emumba::training::student> get_student_reference_shared(const std::string &student_name);
        const std::unique_ptr<emumba::training::student> get_student_reference_unique(const std::string &student_name);
        void set_data(const student &stu_1);
        void print_data(const std::string &) const;
    };
}

#endif