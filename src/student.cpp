// Contains all the Functions of student.h

#include "student.h"
#include <iostream>

emumba::training::student::student()
{
}

emumba::training::student::student(const std::string &name, const std::string &roll_no, const int &age, const float &cgpa)
{
    data.name = name;
    data.roll_no = roll_no;
    data.age = age;
    data.cgpa = cgpa;
};

emumba::training::student::~student()
{
}

void emumba::training::student::set(const std::string &name, const std::string &roll_no, const int &age, const float &cgpa)
{
    data.name = name;
    data.roll_no = roll_no;
    data.age = age;
    data.cgpa = cgpa;
};

std::string emumba::training::student::get_name() const
{
    return data.name;
};

std::string emumba::training::student::get_roll_no() const
{
    return data.roll_no;
};

int emumba::training::student::get_age() const
{
    return data.age;
};

float emumba::training::student::get_cgpa() const
{
    return data.cgpa;
};

int emumba::training::student::get_subject_marks(const std::string &subject)
{
    std::map<std::string, int>::iterator it1;
    it1 = result.find(subject);
    if (it1 == result.end())
    {
        // -1 means subject not found
        return -1;
    }
    else
        return it1->second;
};

void emumba::training::student::set_subject_marks(const std::string &subject, const int &marks)
{
    result[subject] = marks;
};

void emumba::training::student::print_all_marks()
{
    std::cout << "\n"
              << data.name << "'s Marks: \n\n";
    if (result.empty())
    {
        std::cout << "\n No Marks to display for " << data.name << "\n";
    }
    else
    {

        std::map<std::string, int>::iterator it1;
        for (it1 = result.begin(); it1 != result.end(); ++it1)
        {
            std::cout << it1->first << ":    " << it1->second << "\n";
        }
    }
    std::cout << "\n-------------------------------\n";
    return;
};