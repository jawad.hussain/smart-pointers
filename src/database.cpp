#include "database.h"
#include <iostream>
#include <iterator>
#include <memory>
using namespace emumba::training;

emumba::training::database::database()
{
    student stu_1("Jawad", "170488", 22, 4);

    shared_ptr_data["Jawad"] = std::make_shared<student>(stu_1);

    stu_1.set("Adil", "170401", 20, 3.6);
    shared_ptr_data["Adil"] = std::make_shared<student>(stu_1);

    stu_1.set("Ali", "160421", 25, 2.5);
    shared_ptr_data["Ali"] = std::make_shared<student>(stu_1);

    stu_1.set("Haris", "150382", 4, 4);
    shared_ptr_data["Haris"] = std::make_shared<student>(stu_1);

    stu_1.set("Munib", "120483", 5, 4);
    shared_ptr_data["Munib"] = std::make_shared<student>(stu_1);
}

emumba::training::database::~database()
{
    std::cout << "destructor called for database\n";
}

const std::shared_ptr<emumba::training::student> emumba::training::database::get_student_reference_shared(const std::string &student_name)
{
    for (auto &it : shared_ptr_data)
    {
        if (it.first == student_name)
        {
            return it.second;
        }
    }
    return std::shared_ptr<emumba::training::student>(nullptr);
}

const std::unique_ptr<emumba::training::student> emumba::training::database::get_student_reference_unique(const std::string &student_name)
{
    for (auto it : shared_ptr_data)
    {
        if (it.first == student_name)
        {
            return std::make_unique<student>(*it.second);
        }
    }
    return nullptr;
}

void emumba::training::database::set_data(const student &stu_1)
{
    shared_ptr_data[stu_1.get_name()] = std::make_shared<student>(stu_1);
}