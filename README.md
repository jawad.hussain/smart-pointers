# Smart Pointers

##  Discription:
This code demonstrates the use of smart pointers namely shared pointer and unique pointer.
## How to run:
1. Clone git repo using command:
  
    ```bash
    git clone https://gitlab.com/jawad.hussain/smart-pointers.git
    ```
2. Go to root of cloned folder then run the following commands:
   
   ```bash
   mkdir build

   cd build/
   ```
    For Debug mode
    ```bash
    cmake DDEBUG=ON ..
    ```
    For Release mode 
  
    ```bash
    cmake DDEBUG=OFF ..
    ```
    Next make and run:
  
    ```bash
    make
   
    ./app/main
    ```
  
