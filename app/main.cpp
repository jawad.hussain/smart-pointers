/*
NOTE: Shared Pointer can both set and get data where as Unique Poiter can only Get data.
~ Also not using const reference because it does not allow nullptr
*/

#include "database.h"
#include "student.h"
#include <iostream>

void print_student_record(const auto &student_ptr);
void set_subject_marks(const std::shared_ptr<emumba::training::student> &student_ptr, const std::string &subject, const int &marks);
int main()
{
    std::cout << "=================================\n";
    std::cout << "\tSmart Pointers\n";
    std::cout << "=================================\n";

    emumba::training::database db1;

    const std::shared_ptr<emumba::training::student> shared_ptr_1 = db1.get_student_reference_shared("Jawad");
    const std::shared_ptr<emumba::training::student> shared_ptr_2 = db1.get_student_reference_shared("Jawad");
    const std::shared_ptr<emumba::training::student> shared_ptr_3 = db1.get_student_reference_shared("Adil");
    const std::shared_ptr<emumba::training::student> shared_ptr_4 = db1.get_student_reference_shared("Aasdas");

    //----------- Setter Function -----------
    emumba::training::student stu_1("ABC", "17-0001", 22, 3.8);
    db1.set_data(stu_1);
    print_student_record(db1.get_student_reference_unique("ABC"));

    //----------- Setter Subject Marks -----------
    set_subject_marks(shared_ptr_1, "abc", 53);
    set_subject_marks(shared_ptr_1, "bcd", 20);

    //----------- Print Record -----------
    print_student_record(shared_ptr_2);
    print_student_record(shared_ptr_3);
    print_student_record(shared_ptr_4);

    //----------- Getting Data using unique pointer -----------
    const std::unique_ptr<emumba::training::student> unique_ptr_1 = db1.get_student_reference_unique("Jawad");

    print_student_record(unique_ptr_1);
    print_student_record(db1.get_student_reference_unique("Jawad"));

    std::cout << "Count of shared pointer ptr_1: " << shared_ptr_1.use_count() << std::endl;

    std::cout << "\n\n============== END ==============\n";
}

void print_student_record(const auto &student_ptr)
{
    if (student_ptr != nullptr)
    {
        std::cout << "\n---------------------------------\n";
        std::cout << "\tStudent Info\n";
        std::cout << "---------------------------------\n";
        std::cout << "Name: " << student_ptr->get_name() << std::endl;
        std::cout << "Roll no: " << student_ptr->get_roll_no() << std::endl;
        std::cout << "CGPA: " << student_ptr->get_cgpa() << std::endl;
        std::cout << "Age: " << student_ptr->get_age() << std::endl;
        student_ptr->print_all_marks();
    }
    else
        std::cout << "\nStudent Does not exists\n";
}

void set_subject_marks(const std::shared_ptr<emumba::training::student> &student_ptr, const std::string &subject, const int &marks)
{
    if (student_ptr != nullptr)
        student_ptr->set_subject_marks(subject, marks);
    else
        std::cout << "Student Does not exists to enter marks\n";
}
